package com.codecrafterpro.feature.state

import com.codecrafterpro.data.model.Item

data class StateDetailsHolder(
    val isLoading: Boolean = false,
    val data: Item? = null,
    val error: String = ""
)
