package com.codecrafterpro.feature.state

import com.codecrafterpro.data.model.Item

data class StateHolder(
    val isLoading: Boolean = false,
    val data: List<Item>? = null,
    val error: String = ""
)
