package com.codecrafterpro.feature.item_list

import android.os.Bundle
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.codecrafterpro.core.extensions.toJson
import com.codecrafterpro.core.nav_destination.NavDestination
import com.codecrafterpro.data.model.Item

@Composable
fun ItemListScreen(navController: NavController, viewModel: ItemListViewModel = hiltViewModel()) {

    val result = viewModel.state.value

    if (result.isLoading) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    }
    if (result.error.isNotEmpty()) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = result.error)
        }
    }
    result.data?.let { list ->
        LazyColumn {
            items(list) {
                SingleItem(it){
                    navController.navigate(NavDestination.ItemDetails.route+"/${it.owner.login}/${it.name}")
                }
            }
        }
    }
}

@Composable
fun SingleItem(it: Item, onClick:(Item) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clickable { onClick(it) }
        ,
        elevation = 5.dp,
    ) {
        Column(modifier = Modifier.padding(10.dp)) {
            Row() {
                AsyncImage(
                    model = it.owner.avatar_url, contentDescription = null,
                    modifier = Modifier
                        .height(30.dp)
                        .width(30.dp)
                )
                Text(
                    text = it.full_name,
                    modifier = Modifier.padding(10.dp, 0.dp, 0.dp, 0.dp),
                    textAlign = TextAlign.Start,
                    color = Color.Blue,
                    fontSize = 18.sp
                )
            }
            Text(text = it.description)
        }
    }
}
