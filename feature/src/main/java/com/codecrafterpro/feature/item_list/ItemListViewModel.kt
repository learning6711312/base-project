package com.codecrafterpro.feature.item_list

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.feature.state.StateHolder
import com.codecrafterpro.domain.ObserveCollections
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ItemListViewModel @Inject constructor(
    private val observeCollection: ObserveCollections
): ViewModel() {

    val state = mutableStateOf(StateHolder())

    init {
        getItemList()
    }


    private fun getItemList(keyword: String = "android", sort: String = "stars"){
        state.value = StateHolder(isLoading = true)
        observeCollection(keyword, sort).onEach { result ->
            when(result) {
                is Resource.Success -> {
                    state.value = StateHolder(data = result.data.items)
                }
                is Resource.Error -> {
                    state.value = StateHolder(
                        error = result.message ?: "An unexpected error occurred!"
                    )
                }
                is Resource.Loading -> {
                    state.value = StateHolder(isLoading = true)
                }
            }
        }.catch { println(it) }
            .launchIn(viewModelScope)
    }
}