package com.codecrafterpro.feature.item_details


import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.codecrafterpro.core.extensions.formatDate

@Composable
fun ItemDetailsScreen( viewModel: ItemsDetailsViewModel = hiltViewModel()) {
    val result = viewModel.state.value

    if (result.isLoading) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    }
    if (result.error.isNotEmpty()) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = result.error)
        }
    }
    result.data?.let { item ->
        Column( modifier = Modifier
            .padding(10.dp)
            .verticalScroll(rememberScrollState(),)
        ) {
            AsyncImage(model = item.owner.avatar_url, contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(350.dp))
            Text(text = "Owner Name: ${item.name}", modifier = Modifier.padding(top = 10.dp))
            Text(text = "Last Modified: ${item.updated_at.formatDate()}")
            Text(text = item.description, modifier = Modifier.padding(top = 20.dp))
        }
    }

}