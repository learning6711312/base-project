package com.codecrafterpro.feature.item_details

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.domain.ObserveItemDetails
import com.codecrafterpro.feature.state.StateDetailsHolder
import com.codecrafterpro.feature.state.StateHolder
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemsDetailsViewModel @Inject constructor(
    private val observeItemDetails: ObserveItemDetails,
    savedStateHandle: SavedStateHandle):ViewModel() {

    val state = mutableStateOf(StateDetailsHolder())

    init {
        viewModelScope.launch {
            savedStateHandle.getStateFlow("ownerName", "").collectLatest {ownerName->
                savedStateHandle.getStateFlow("repoName", "").collectLatest { repoName ->
                    getItemDetails(ownerName, repoName)
                }
            }
        }
    }

    private fun getItemDetails(ownerName: String, repoName: String) = viewModelScope.launch {
        state.value = StateDetailsHolder(isLoading = true)

        observeItemDetails(ownerName, repoName).onEach { result ->
            when(result) {
                is Resource.Success -> {
                    state.value = StateDetailsHolder(data = result.data)
                }
                is Resource.Error -> {
                    state.value = StateDetailsHolder(
                        error = result.message ?: "An unexpected error occurred!"
                    )
                }
                is Resource.Loading -> {
                    state.value = StateDetailsHolder(isLoading = true)
                }
            }
        }.catch { println(it) }
            .launchIn(viewModelScope)
    }
}