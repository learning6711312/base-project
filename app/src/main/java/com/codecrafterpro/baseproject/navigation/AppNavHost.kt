package com.codecrafterpro.baseproject.navigation

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.codecrafterpro.core.nav_destination.NavDestination
import com.codecrafterpro.feature.item_details.ItemDetailsScreen
import com.codecrafterpro.feature.item_list.ItemListScreen

@Composable
fun AppNavHost(navHostController: NavHostController) {

    NavHost(navController = navHostController,
        startDestination = NavDestination.ItemList.route) {

        composable(NavDestination.ItemList.route) {
            ItemListScreen(navHostController)
        }

        composable(NavDestination.ItemDetails.route+"/{ownerName}/{repoName}") {
            val ownerName = it.arguments?.getString("ownerName")
            val repoName = it.arguments?.getString("repoName")

            ItemDetailsScreen()
        }
    }

}