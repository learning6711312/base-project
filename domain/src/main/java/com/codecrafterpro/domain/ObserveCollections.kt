package com.codecrafterpro.domain

import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.data.model.CollectionResponse
import com.codecrafterpro.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveCollections @Inject constructor(
    private val repository: Repository
){
    operator fun invoke(keyword: String, sort: String): Flow<Resource<CollectionResponse>> {
        return repository.getCollections(keyword, sort).map {
            return@map it
        }.flowOn(Dispatchers.IO)
    }
}