package com.codecrafterpro.domain.di

import com.codecrafterpro.data.repository.Repository
import com.codecrafterpro.domain.ObserveCollections
import com.codecrafterpro.domain.ObserveItemDetails
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ObserversModule {

    @Provides
    fun observeCollections(repository: Repository) : ObserveCollections {
        return ObserveCollections(repository)
    }

    @Provides
    fun observerItemDetails(repository: Repository): ObserveItemDetails {
        return ObserveItemDetails(repository)
    }
}