package com.codecrafterpro.domain

import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.data.model.CollectionResponse
import com.codecrafterpro.data.model.Item
import com.codecrafterpro.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveItemDetails  @Inject constructor(
    private val repository: Repository
){
    operator fun invoke(ownerName: String, repoName: String): Flow<Resource<Item>> {
        return repository.getItemDetails(ownerName, repoName).map {
            return@map it
        }.flowOn(Dispatchers.IO)
    }
}