package com.codecrafterpro.core.extensions

import org.junit.Assert.assertEquals
import org.junit.Test


class ExtensionsUnitTest {

    @Test
    fun testFormatDate() {
        val dateString = "2024-03-23T12:34:56Z"
        val expectedOutput = "03-23-2024 12:56"
        val result = dateString.formatDate()
        assertEquals(expectedOutput, result)
    }
}
