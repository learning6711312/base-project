package com.codecrafterpro.core.extensions

import java.text.SimpleDateFormat

fun String.formatDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    val formatterOutput = SimpleDateFormat("MM-dd-yyyy HH:ss")
    return formatterOutput.format(sdf.parse(this))
}