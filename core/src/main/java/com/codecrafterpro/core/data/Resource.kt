package com.codecrafterpro.core.data

sealed class Resource<T : Any> {
    data class Success<T : Any>(val data: T) : Resource<T>()
    data class Error<T : Any>(val code: Int? = 0, val message: String?) : Resource<T>()
    data class Loading<T : Any>(val msg: String? = null) : Resource<T>()
}
