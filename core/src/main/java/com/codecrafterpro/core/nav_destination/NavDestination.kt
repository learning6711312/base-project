package com.codecrafterpro.core.nav_destination

sealed class NavDestination(val route: String){
    object ItemList: NavDestination("item_list")
    object ItemDetails: NavDestination("item_details")
}
