package com.codecrafterpro.data.source

import com.codecrafterpro.data.network.ApiService

interface DataSource {

    fun getApiService(): ApiService
}