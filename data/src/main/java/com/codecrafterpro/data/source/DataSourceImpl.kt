package com.codecrafterpro.data.source

import com.codecrafterpro.data.network.ApiService
import javax.inject.Inject

class DataSourceImpl @Inject constructor(
    private val apiService: ApiService
) : DataSource {
    override fun getApiService(): ApiService {
        return apiService
    }
}