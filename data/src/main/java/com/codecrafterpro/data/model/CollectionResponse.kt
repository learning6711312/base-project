package com.codecrafterpro.data.model

data class CollectionResponse(
    val incomplete_results: Boolean,
    val items: List<Item>,
    val total_count: Int
): java.io.Serializable