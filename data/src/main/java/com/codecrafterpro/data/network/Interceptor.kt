package com.codecrafterpro.data.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class CustomInterceptor(private val packageName: String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest: Request = chain.request()
        val modifiedRequest: Request = originalRequest.newBuilder()
            .header("package", packageName)
            .build()

        return chain.proceed(modifiedRequest)
    }

}