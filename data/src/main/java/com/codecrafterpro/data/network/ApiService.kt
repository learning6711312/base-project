package com.codecrafterpro.data.network

import com.codecrafterpro.data.model.CollectionResponse
import com.codecrafterpro.data.model.Item
import com.codecrafterpro.data.network.Endpoints.REPOS
import com.codecrafterpro.data.network.Endpoints.SEARCH_REPOSITORIES
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET(SEARCH_REPOSITORIES)
    suspend fun getItems(
        @Query("q") keyword: String,
        @Query("sort") sort: String
    ):CollectionResponse

    @GET("$REPOS/{ownerName}/{repoName}")
    suspend fun getReposDetails(
        @Path("ownerName") ownerName: String,
        @Path("repoName") repoName: String
    ): Item
}