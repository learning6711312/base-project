package com.codecrafterpro.data.network

object Endpoints {
    const val SEARCH_REPOSITORIES = "search/repositories"
    const val REPOS = "repos"
}