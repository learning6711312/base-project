package com.codecrafterpro.data.di

import com.codecrafterpro.core.BuildConfig
import com.codecrafterpro.core.util.Constants
import com.codecrafterpro.data.network.ApiService
import com.codecrafterpro.data.network.CustomInterceptor
import com.codecrafterpro.data.repository.Repository
import com.codecrafterpro.data.repository.RepositoryImpl
import com.codecrafterpro.data.source.DataSource
import com.codecrafterpro.data.source.DataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object Network {

    val client = OkHttpClient.Builder()
        .addInterceptor(CustomInterceptor(Constants.PACKEGE_NAME))
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun provideDataSource(apiService: ApiService): DataSource {
        return DataSourceImpl(apiService)
    }

    @Provides
    fun provideRepository(dataSource: DataSource): Repository {
        return RepositoryImpl(dataSource)
    }
}