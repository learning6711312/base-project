package com.codecrafterpro.data.repository

import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.data.model.CollectionResponse
import com.codecrafterpro.data.model.Item
import com.codecrafterpro.data.source.DataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val dataSource: DataSource
): Repository {
    override fun getCollections(keyword: String, sort: String): Flow<Resource<CollectionResponse>> = flow {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(data = dataSource.getApiService().getItems(keyword, sort)))
        } catch (e: java.lang.Exception) {
            emit(Resource.Error(message = e.message.toString()))
        }
    }

    override fun getItemDetails(ownerName: String, repoName: String): Flow<Resource<Item>> = flow {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(data = dataSource.getApiService().getReposDetails(ownerName, repoName)))
        } catch (e: java.lang.Exception) {
            emit(Resource.Error(message = e.message.toString()))
        }
    }
}