package com.codecrafterpro.data.repository

import com.codecrafterpro.core.data.Resource
import com.codecrafterpro.data.model.CollectionResponse
import com.codecrafterpro.data.model.Item
import kotlinx.coroutines.flow.Flow

interface Repository {

    fun getCollections(keyword: String, sort: String) : Flow<Resource<CollectionResponse>>

    fun getItemDetails(ownerName: String, repoName: String) : Flow<Resource<Item>>
}